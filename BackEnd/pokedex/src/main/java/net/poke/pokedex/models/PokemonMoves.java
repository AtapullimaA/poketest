package net.poke.pokedex.models;

import java.util.ArrayList;

public class PokemonMoves {
	private Move move;
	private ArrayList<VerionGroupDetails> version_group_details;
	
	public Move getMove() {
		return move;
	}
	public void setMove(Move move) {
		this.move = move;
	}
	public ArrayList<VerionGroupDetails> getVersion_group_details() {
		return version_group_details;
	}
	public void setVersion_group_details(ArrayList<VerionGroupDetails> version_group_details) {
		this.version_group_details = version_group_details;
	}
}
