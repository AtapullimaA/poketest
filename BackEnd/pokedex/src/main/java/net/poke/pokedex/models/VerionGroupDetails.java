package net.poke.pokedex.models;

public class VerionGroupDetails {
	private int level_learned_at;
	private ModeLearn move_learn_method;
	
	public int getLevel_learned_at() {
		return level_learned_at;
	}
	public void setLevel_learned_at(int level_learned_at) {
		this.level_learned_at = level_learned_at;
	}
	public ModeLearn getMove_learn_method() {
		return move_learn_method;
	}
	public void setMove_learn_method(ModeLearn move_learn_method) {
		this.move_learn_method = move_learn_method;
	}
}
