package net.poke.pokedex.models;

import java.util.ArrayList;

public class Pokemon {
	private int id;
	private int base_experience;
	private int height;
	private boolean is_default;
	private int order;
	private int weight;
	
	private ArrayList<PokemonAbility> abilities;
	private ArrayList<Forms> forms;
	private ArrayList<VersionGameIndex> game_indices;
	
	private String location_area_encounters;
	private ArrayList<PokemonMoves> moves;
	
	private Species species;
	private Sprites sprites;
	
	private ArrayList<Stats> stats;
	private ArrayList<PokemonTypes> types;
	
	private ArrayList<String> held_items;
	private ArrayList<String> past_types;
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getBase_experience() {
		return base_experience;
	}
	public void setBase_experience(int base_experience) {
		this.base_experience = base_experience;
	}
	public int getHeight() {
		return height;
	}
	public void setHeight(int height) {
		this.height = height;
	}
	public boolean isIs_default() {
		return is_default;
	}
	public void setIs_default(boolean is_default) {
		this.is_default = is_default;
	}
	public int getOrder() {
		return order;
	}
	public void setOrder(int order) {
		this.order = order;
	}
	public int getWeight() {
		return weight;
	}
	public void setWeight(int weight) {
		this.weight = weight;
	}
	public ArrayList<PokemonAbility> getAbilities() {
		return abilities;
	}
	public void setAbilities(ArrayList<PokemonAbility> abilities) {
		this.abilities = abilities;
	}
	public ArrayList<Forms> getForms() {
		return forms;
	}
	public void setForms(ArrayList<Forms> forms) {
		this.forms = forms;
	}
	public ArrayList<VersionGameIndex> getGame_indices() {
		return game_indices;
	}
	public void setGame_indices(ArrayList<VersionGameIndex> game_indices) {
		this.game_indices = game_indices;
	}
	public String getLocation_area_encounters() {
		return location_area_encounters;
	}
	public void setLocation_area_encounters(String location_area_encounters) {
		this.location_area_encounters = location_area_encounters;
	}
	public ArrayList<PokemonMoves> getMoves() {
		return moves;
	}
	public void setMoves(ArrayList<PokemonMoves> moves) {
		this.moves = moves;
	}
	public Species getSpecies() {
		return species;
	}
	public void setSpecies(Species species) {
		this.species = species;
	}
	public Sprites getSprites() {
		return sprites;
	}
	public void setSprites(Sprites sprites) {
		this.sprites = sprites;
	}
	public ArrayList<Stats> getStats() {
		return stats;
	}
	public void setStats(ArrayList<Stats> stats) {
		this.stats = stats;
	}
	public ArrayList<PokemonTypes> getTypes() {
		return types;
	}
	public void setTypes(ArrayList<PokemonTypes> types) {
		this.types = types;
	}
	public ArrayList<String> getHeld_items() {
		return held_items;
	}
	public void setHeld_items(ArrayList<String> held_items) {
		this.held_items = held_items;
	}
	public ArrayList<String> getPast_types() {
		return past_types;
	}
	public void setPast_types(ArrayList<String> past_types) {
		this.past_types = past_types;
	}
	
	
	
}
	
