package net.poke.pokedex.models;

public class PokemonTypes {
	private int slot;
	private PType type;
	public int getSlot() {
		return slot;
	}
	public void setSlot(int slot) {
		this.slot = slot;
	}
	public PType getType() {
		return type;
	}
	public void setType(PType type) {
		this.type = type;
	}
}
