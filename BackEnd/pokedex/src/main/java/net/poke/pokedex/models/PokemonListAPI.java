package net.poke.pokedex.models;

import java.util.ArrayList;


public class PokemonListAPI {
	private int count;
	private String next;
	private String previous;
	private ArrayList<Results> results;
	
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	public String getNext() {
		return next;
	}
	public void setNext(String next) {
		this.next = next;
	}
	public String getPrevious() {
		return previous;
	}
	public void setPrevious(String previous) {
		this.previous = previous;
	}
	public ArrayList<Results> getResults() {
		return results;
	}
	public void setResults(ArrayList<Results> results) {
		this.results = results;
	}
	@Override
	public String toString() {
		return new com.google.gson.Gson().toJson(this);
	}	
}
