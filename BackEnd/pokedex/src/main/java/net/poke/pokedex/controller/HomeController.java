package net.poke.pokedex.controller;

import java.io.IOException;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;

import net.poke.pokedex.models.*;
import net.poke.pokedex.utilitarios.HttpConection;

@RestController
@CrossOrigin
@RequestMapping("/Pokemon")
public class HomeController {

	public static String apiurl ="https://pokeapi.co/api/v2/pokemon/";
	
	@GetMapping("/all")
	public PokemonListAPI getPokemons() throws IOException {
		PokemonListAPI pokemonList = new PokemonListAPI();
		String jsonObject = null;

		jsonObject = new HttpConection().CallPokeApi(apiurl);

		pokemonList = new Gson().fromJson(jsonObject.toString(), PokemonListAPI.class);
		System.out.println(pokemonList);
		return pokemonList;
		
	}
	
	@GetMapping("/all/{id}")
	public Pokemon getPokemons(@PathVariable("id") String idPokemon ) throws IOException {
		Pokemon pokemonDetail = new Pokemon();
		String jsonObject = null;

		jsonObject = new HttpConection().CallPokeApi(apiurl+idPokemon);

		pokemonDetail = new Gson().fromJson(jsonObject.toString(), Pokemon.class);
		
		return pokemonDetail;
		
	}
	
}
