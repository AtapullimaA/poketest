package net.poke.pokedex.models;

public class OtherSprites {
	private DreamWorld dream_world;
	private OfficialArtWork officialartwork;
	
	public DreamWorld getDream_world() {
		return dream_world;
	}
	public void setDream_world(DreamWorld dream_world) {
		this.dream_world = dream_world;
	}
	public OfficialArtWork getOfficialartwork() {
		return officialartwork;
	}
	public void setOfficialartwork(OfficialArtWork officialartwork) {
		this.officialartwork = officialartwork;
	}
	
	
}
