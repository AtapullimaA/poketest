package net.poke.pokedex.utilitarios;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

public class HttpConection {
	
	public String CallPokeApi(String apiurl) throws IOException{

        URL url = new URL(apiurl);
        HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();
        connection.setRequestMethod("GET");
        connection.setRequestProperty("User-Agent", "Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10.4; en-US; rv:1.9.2.2) Gecko/20100316 Firefox/3.6.2");
        connection.setConnectTimeout(100000);
        connection.connect();

        System.out.println("Codigo : " + connection.getResponseCode());
        BufferedReader recv = new BufferedReader(new InputStreamReader(connection.getInputStream()));
        System.out.println(recv);
        String outputLine;
        StringBuffer response = new StringBuffer();
        while ((outputLine = recv.readLine()) != null) {
            response.append(outputLine);
        }
        System.out.println(response);
        
        return response.toString();
    }

}